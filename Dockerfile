FROM node:16-alpine3.11
COPY ./build /app/build
WORKDIR /app
# RUN npm i 
RUN npm i serve -g
EXPOSE 3000
ENTRYPOINT [ "serve", "-s", "build" ]

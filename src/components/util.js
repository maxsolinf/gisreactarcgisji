import Map from '@arcgis/core/Map';
import View from '@arcgis/core/views/MapView'

export default function createView(
    container,
    viewPoperties={zoom:10, center: [-70, 18.9]}, 
    mapProperties={basemap: "streets"}
    ){
        let map = new Map(mapProperties)
        const view = new View({
            map: map,
            ...viewPoperties,
            container
        })
        console.log(view)
        return view
}

import { useState } from "react"
import { useEffect } from "react/cjs/react.development"

export const useFetch = (uri, method, body={})=>{
    const [err, setErr] = useState()
    const [load, setLoad] = useState(false)
    const [res, setRes] = useState()

    useEffect(()=>{
        if(!uri) return
        setLoad(true)
        // for ji consultant must ve 'value:"latitude, longitude"'
        fetch(uri, {method: method, body:body})
        .then(res=>res.json())
        .then(res=>{
            setRes(res);
            console.log('res Ready \n ' + res)
        })
        .catch(err=>setErr(err))
        setLoad(false)
    },[uri, body]) //eslint-disable-line

    return [load, err, res]
}
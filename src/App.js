import { Webmap } from './features/mapView/Webmap';
import './App.css';
import { UiProvider } from './context/uiContext';
import { SelectionProvider } from './context/selectionContext';
import { InteractionProvider } from './context/interactionContext';

function App() {
  return (
    <div className="App">
      <UiProvider>
      <SelectionProvider>
        <InteractionProvider>
          <Webmap />
        </InteractionProvider>
    </SelectionProvider>
    </UiProvider>
    </div>
  );
}

export default App;

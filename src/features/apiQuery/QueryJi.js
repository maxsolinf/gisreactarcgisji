import { useFetch } from "../../components/useFetch";
import { useSelection } from "../../context/selectionContext";

export const QueryJI = ({
    renderSuccess=(<></>),
    loadingFallback=(<p>loading...</p>),
    error=(<pre>error</pre>)
})=>{
    const uri = "https://servicios.ri.gob.do/ConsultaGeografica/ConsultarContorno"
    const method = 'post'
    const {selection} = useSelection()
    const params = new URLSearchParams();
    params.append("value", `${selection.latitude},${selection.longitude}`)
    const [load, err, res] = useFetch(uri, method, params)
    console.log("llamando api")
    return(
            load ? loadingFallback : err ? error : res ? renderSuccess: <>noinfo</>
    )
}
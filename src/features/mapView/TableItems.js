import {  Grid, IconButton, Paper, Tooltip } from "@mui/material"
import { useEffect, useState } from "react"
import { styled } from '@mui/material/styles';
import FeatureTable from "@arcgis/core/widgets/FeatureTable";
import TocIcon from '@mui/icons-material/Toc';
import Box from '@mui/material/Box';
//import Grid from '@material-ui/core/Grid'

const StyledIconButton = styled(IconButton)({
    position:"absolute",
    top: "2%",
    left: "2%",
})
const StyledBox = styled(Box)({
    position: "absolute",
    top: "8%",
    left: '1%',
    height: "90vh",
    width: (window.innerWidth > 600) ? '43%': '78%',
})

// const tableR = new FeatureTable()
// const tableH = new FeatureTable()

export const TableItems = ({view, features})=>{
    const [layer, setLayer] = useState()
    const [visible, setVisible] = useState(false)
    const [color, setColor] = useState('primary')
    const [currLayer, setCurrLayer] = useState()

    const handleChange = (e)=>{
        setLayer(e.target.value)
    }
    useEffect(()=>{
        if(!layer)return
        console.log(layer)
        setCurrLayer(features.filter(val=>val.title === layer)[0])
        console.log("layer set")
    },[layer])

    useEffect(()=>{
        if(!currLayer)return
        console.log(currLayer.graphics)

    },[currLayer])

    return(
        <>
        <Tooltip title="Turn On and Off the distinct Layers on the map ">
        <StyledIconButton color={color} aria-label="Layer switch tool" onClick={()=>setVisible(!visible)}>
            <TocIcon/>
        </StyledIconButton>
        </Tooltip>
        <StyledBox sx={{
            display: visible?'inline-flex':'none'
        }}>
            <Grid container spacing={1}>
              <Paper>
                  <select value={layer} name="layer"  onChange={handleChange}>
                      {features && features.map((val, idx)=>{
                          return(
                              <option value={val && val.title} key={val && idx.toString() + val.title}>{val && val.title}</option>
                          )
                      })}

                  </select>
              </Paper>
            </Grid>
            <Grid id="table">

            </Grid>
        </StyledBox>
        </>
    )
}
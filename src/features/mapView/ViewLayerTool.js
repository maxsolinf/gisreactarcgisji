import { IconButton, Tooltip } from "@mui/material"
import { useEffect, useState } from "react"
import { useUi } from "../../context/uiContext"
import VisibilityIcon from '@mui/icons-material/Visibility';
import { styled } from '@mui/material/styles';
import LayerList from "@arcgis/core/widgets/LayerList";


const StyledIconButton = styled(IconButton)({
    position:"absolute",
    bottom: '7%',
    left: (window.innerWidth > 600) ? '4%': '18%',
})

let layerList = new LayerList( {
    creationMode: 'single',
    visibleElements: {
        settingsMenu: false,
        selectionTools:{
            "lasso-selection": false,
            "rectangle-selection": false,
        },
        createTools:{
            point: false,
            polyline: false,
            circle: false,
            rectangle: false
        },

}}
)
export const ViewLayerToolUi = ({view})=>{
    const {layerView, setLayerView} = useUi()
    const [color, setColor] = useState('primary')
    if(view) {
        layerList.view = view
        layerList.visible = layerView
    }
    const handleClick = ()=>{
        setLayerView && setLayerView(!layerView)
        if(color === 'primary'){
            setColor('secondary')
        }
        else{
        setColor('primary')}
    }
    useEffect(()=>{
        if(!view) return
        if(!layerView){
            console.log('removig layer list.')
            view.ui.remove(layerList)
        }
        else if(layerView){
            console.log('creating sketch')
            view.ui.add(layerList, {position: 'top-left', index: 3})
        }
        
    },[layerView]) //eslint-disable-line
    return(
        <Tooltip title="Turn On and Off the distinct Layers on the map ">
        <StyledIconButton color={color} aria-label="Layer switch tool" onClick={handleClick}>
            <VisibilityIcon />
        </StyledIconButton>
        </Tooltip>
    )
}
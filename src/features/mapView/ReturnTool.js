import KeyboardReturnIcon from '@mui/icons-material/KeyboardReturn';
import { IconButton, Tooltip } from '@mui/material';
import { styled } from '@mui/material/styles';
import { useState } from 'react';
import { useSelection } from "../../context/selectionContext";


const StyledIconButton = styled(IconButton)({
    position:"absolute",
    bottom: '2%',
    left: (window.innerWidth > 600) ? '2%': '9%',
})

export const ReturnTool = ({view})=>{
    const {selection, setSelection} = useSelection()
    const [color] = useState('error')
    const params = new URLSearchParams()
    params.append('value', "18.408871982799656,-70.12983313539176")
    //const [load, err, res] = useFetch("https://servicios.ri.gob.do/ConsultaGeografica/ConsultarContorno", 'post' ,params)

    const handleClick = ()=>{
        setSelection({...selection, latitude: 18.5, longitude: -70})
        view.goTo({center: [-70, 18.5], zoom: 10})
    }

    return(
        <Tooltip title="Reset the map to its initial state.">
        <StyledIconButton color={color} onClick={handleClick}>
            <KeyboardReturnIcon />
        </StyledIconButton>
        </Tooltip>
    )
}
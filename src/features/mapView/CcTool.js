import CoordinateConversion from "@arcgis/core/widgets/CoordinateConversion";
import { IconButton, Tooltip } from "@mui/material"
import { useEffect, useState } from "react"
import { useUi } from "../../context/uiContext"
import LocationSearchingIcon from '@mui/icons-material/LocationSearching';
import { styled } from '@mui/material/styles';


const StyledIconButton = styled(IconButton)({
    position:"absolute",
    bottom: '7%',
    left: (window.innerWidth > 600) ? '2%': '9%',
})
let ccui = new CoordinateConversion()

export const CCTool = ({view})=>{
    const {cc, setCC} = useUi()
    const [color, setColor] = useState('primary')
    if(view) {
        ccui.view = view
        ccui.visible = cc
    }
    const handleClick = ()=>{
        setCC && setCC(!cc)
        if(color==='primary'){
            setColor('secondary')
        }
        else{
            setColor('primary')
        }
    }
    useEffect(()=>{
        if(!view) return
        if (!cc){
            console.log("removing Coordinates converter")
            view.ui.remove(ccui)
        }
        else {
            console.log("adding Coordinates converter")
            view.ui.add(ccui, {position: 'top-left', index: 2})
            
        }
    },[cc]) //eslint-disable-line
    return(
        <Tooltip title="Coordinates Converter Tool">
        <StyledIconButton color={color} onClick={handleClick}>
            <LocationSearchingIcon />
        </StyledIconButton>
        </Tooltip>
    )
}
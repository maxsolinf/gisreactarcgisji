import { IconButton } from "@mui/material"
import ManageSearchIcon from '@mui/icons-material/ManageSearch';
import { styled } from "@mui/system";
import { useUi } from "../../context/uiContext";
import { useSelection } from "../../context/selectionContext";
import { useEffect, useState } from "react";
import { useInteraction } from "../../context/interactionContext";

const StyledIconButton = styled(IconButton)({
    position: 'absolute',
    bottom: "5%",
    left: "45%",
    backgroundColor: 'hsla(255, 95%, 95%, 0.5)'
})

export const GetInfoTool = ()=>{

    const {zoom} = useUi()
    const {selection} = useSelection()
    const [result, setResult] = useState()
    const {load, changeLoading, setErr, setData} = useInteraction()
    
    useEffect(()=>{
        if(!result)return
        if(!result.polygons) return console.log("Polygons not obtained")
        let resultantes = new Map()
        let historicas = new Map()
        result.polygons.map(value=>{
            if(value.capa === "Parcelas Ley 108-05"){
                return resultantes.set(value.posicional, value)
            }
            else{
                return historicas.set(`${value.parcela}: ${value.municipio}`, value)
            }
        })
        setData({resultantes, historicas})
        // console.log(data.resultantes)
    },[result])//eslint-disable-line
    
    const handleClick = ()=> {
        queryApi()
    }
    const queryApi =()=>{
        // Url por defecto que usaremos
        const uri = "https://servicios.ri.gob.do/ConsultaGeografica/ConsultarContorno"
        // Metodo de consulta
        const method = 'post'
        // parametros de consulta en el body
        let params = new URLSearchParams()
        params.append('value', `${selection.latitude},${selection.longitude}`)
        changeLoading(true)
        fetch(uri, {method, body:params})
        .then(res=>res.json())
        .then(res=>setResult(res))
        .then(()=>changeLoading(false))
        .catch(err=>{
            setErr(err);
        })
        
    }
    return (
        <>
        <StyledIconButton sx={{
            display: (zoom > 13 && selection.latitude !== 18 && selection.longitude !== -70)?'inline-flex':'none'}} color="success" disabled={load} onClick={handleClick}>
            <ManageSearchIcon sx={{fontSize: 50}} />
        </StyledIconButton> 
</>
        
    )
}
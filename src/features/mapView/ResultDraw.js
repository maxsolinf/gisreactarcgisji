import {useInteraction} from '../../context/interactionContext'
import { useEffect } from 'react';


export const ResultDraw = ({view, hLayer, rLayer})=>{
    const {data} = useInteraction()

    useEffect(()=>{
        if(!data)return
        rLayer.removeAll()
        hLayer.removeAll()
        const resultantes = data['resultantes']
        const historicas = data['historicas']
        let listaHistoricas = []
        let listaResultantes = []
        if (historicas.size > 0){
            listaHistoricas = [...historicas.values()]
        }
        if (resultantes.size > 0){
            listaResultantes = [...resultantes.values()]
        }
        let polygonsR = []
        let polygonsH = []
        function resolve(item){
            // si es historica es true si es resultante es false
            let tipo = (item.capa === "Parcelas Ley 1542")? true: false
            // console.log(item.puntos.map(value=>[value.x, value.y]))
            let poly = {
                type: 'polygon',
                rings: item.puntos.map(value=>[Number(value.x), Number(value.y)]),
                spatialReference: { wkid: 4326 }
            };
            let symbol = {
                type: "simple-fill",
                color: tipo?[71, 51, 224, 0.9]: [70, 70, 180, 0.7],
                style: tipo?'diagonal-cross':'backward-diagonal',
                outline: {
                    color: tipo?'blue':'red',
                    width: 1
                }
        }
        const popupTemplate = {
            title: "{Name}",
            content: [{
                type: "fields",
                fieldInfos: [
                    {
                        fieldName: "posicional",
                        label: "Posicional",
                    },
                    {
                        fieldName: "numeroExpediente",
                        label: "Expediente",
                    },
                    {
                        fieldName: "operacion",
                        label: "Operacion",
                    },
                    {
                        fieldName: "municipio",
                        label: "Municipio",
                    },
                    {
                        fieldName: "provincia",
                        label: "Provincia",
                    },
                    {
                        fieldName: "dc",
                        label: "DC",
                    },
                    {
                        fieldName: "Inscripcion",
                        label: "Inscripcion"
                    },
                    {
                        fieldName: "capa",
                        label: "Ley",
                    },
                    {
                        fieldName: "Area",
                        label: "Area",
                    },
                   
                ]
            }]
         }

        let polygonAtt = {
            Name: tipo?item.parcela:item.posicional,
            Area: Number(item.area|| 0).toFixed(3) + ' m2',
            Inscripcion: new Date(item.fechaInscripcion|| Date.now()).toDateString(),
            ...item
        }
        return {
            geometry: poly,
            symbol: symbol,
            attributes: polygonAtt,
            popupTemplate
        }}
        
        listaResultantes.forEach((value)=>{
            polygonsR.push(resolve(value))
        })
        listaHistoricas.forEach(value=>{
            polygonsH.push(resolve(value))
        })
        hLayer.addMany(polygonsH)
        rLayer.addMany(polygonsR)
        // console.log(polygonsR)
        
    },[data])//eslint-disable-line

    return(
    <>
    </>
    )
}
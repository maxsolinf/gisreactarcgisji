import Search from "@arcgis/core/widgets/Search"
import { IconButton, Tooltip } from "@mui/material"
import { useEffect, useState } from "react"
import { useUi } from "../../context/uiContext"
import SearchIcon from '@mui/icons-material/Search';
import { styled } from '@mui/material/styles';
import { useSelection } from "../../context/selectionContext";


const StyledIconButton = styled(IconButton)({
    position:"absolute",
    bottom: '7%',
    left: 0,
})
let searchui = new Search()

export const SearchTool = ({view})=>{
    const {search, setSearch} = useUi()
    const [color, setColor] = useState('primary')
    const {selection, setSelection} = useSelection()
    if(view) {
        searchui.view = view
        searchui.visible = search
    }
    const handleClick = ()=>{
        setSearch && setSearch(!search)
        if(color==='primary'){
            setColor('secondary')
        }
        else{
            setColor('primary')
        }
    }
    useEffect(()=>{
        if(!view) return
        if (!search){
            console.log("removing Search")
            searchui.clear()
            view.ui.remove(searchui)

        }
        else if(search ){
            console.log("adding Search")
            view.ui.add(searchui, {position: 'top-left', index: 0})
            searchui.on('search-complete',(e)=>{
                if(e.results.length > 0){
                    let {latitude, longitude} = e.results[0].results[0].target.geometry.centroid
                    setSelection({...selection, latitude, longitude})
                    console.log(`Obtenidas Latitude ${latitude}, ${longitude}`)
                }
            })
        }
    },[search]) //eslint-disable-line
    return(
        <Tooltip title="Geo Search, take you to the direction you search in the map.">
        <StyledIconButton color={color} onClick={handleClick}>
            <SearchIcon />
        </StyledIconButton>
        </Tooltip>
    )
}
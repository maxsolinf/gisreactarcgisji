import React, { useEffect, useRef, useState } from 'react';
import Map from '@arcgis/core/Map';
import View from '@arcgis/core/views/MapView'
import Graphic from '@arcgis/core/layers/GraphicsLayer';
import { DrawToolUi } from './DrawTool';
import { SearchTool } from './SearchTool';
import { CCTool } from './CcTool';
import { ReturnTool } from './ReturnTool';
import { GetInfoTool } from './GetInfoTool';
import { useUi } from '../../context/uiContext';
import { useSelection } from '../../context/selectionContext';
import { ResultDraw } from './ResultDraw';
import { ViewLayerToolUi } from './ViewLayerTool';
export const Webmap = ()=>{
    const viewDiv = useRef(null)
    // Los estadosno actualizan los cambios en el render de los deep objects por lo que si necesitas algo tomalo en el momento el mutara 
    // constantemente solo hace falta acceder a las propiedades del objeto
    const [localView, setLocalView] = useState(null)
    // Los layers se agregan a nivel de creacion de mapa
    const [graphicsLayer, setGraphicsLayer] = useState(null)
    const [resultGraphicsLayer, setResultGraphicsLayer] = useState(null)
    const [historicGraphicsLayer, setHistoricGraphicsLayer] = useState(null)
    const {zoom, setZoom} = useUi()
    const {selection, setSelection} = useSelection()
    
    // En el primer render hacemos esto
    useEffect(()=>{
        const graphicsLayer = new Graphic({title: 'Graphics', visible: true})
        setGraphicsLayer(graphicsLayer)
        const resultsLayers = new Graphic(
            {
                title: 'Resultantes', 
                visible: false,
        })
        setResultGraphicsLayer(resultsLayers)
        const historicLayer = new Graphic({title: 'Historicas', visible: false})
        setHistoricGraphicsLayer(historicLayer)
        let map = new Map({
            basemap: 'streets', 
            layers: [historicLayer, resultsLayers, graphicsLayer]
        })
        setLocalView(new View({
            container: viewDiv.current,
            map:map,
            zoom: 10,
            center: [-70, 18.5],
        }))
       return localView && localView.destroy()
    },[]) //eslint-disable-line

    // luego que creamos el mapa creamos la ui si todo existe
    useEffect(()=>{
        if(!localView) return
        if(!localView.ui) return
        
        localView.when(
            ()=>{
                console.log('view is ready')
                localView.ui.remove('zoom')
                localView.on('pointer-move', (e)=>{
                    if(!zoom){
                        setZoom(localView.zoom)
                    }
                    else if(zoom && localView.zoom !== zoom){
                        setZoom(localView.zoom)
                    }
                })
                localView.on('drag', (e)=>{
                    if(e.action === 'end'){
                        if(localView.zoom >= 13){
                            const {latitude, longitude} = localView.center
                            setTimeout(()=>setSelection({...selection, latitude, longitude}), 1500)
                        }
                    }
                })
            
            })
        // Funcion externa que genera los ui con sus respectivos posicionamientos y evnetos
       // Mapui(localView, graphicsLayer)
    },[localView])//eslint-disable-line
   
    return (
        <>
            <div className="viewDiv" ref={viewDiv} style={{width: '100vw', height: '100vh'}}>
                {/* <Mapui view={localView} graphicsLayer={graphicsLayer} /> */}
                <SearchTool view={localView} />
                <DrawToolUi view={localView} layer={graphicsLayer}/>
                <CCTool view={localView} />
                <ReturnTool view={localView} />
                <GetInfoTool />
                <ResultDraw view={localView} rLayer={resultGraphicsLayer} hLayer={historicGraphicsLayer} />       
                <ViewLayerToolUi view={localView}/>    
            </div>
        </>
    )
}
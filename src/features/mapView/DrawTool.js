import Sketch from "@arcgis/core/widgets/Sketch"
import { IconButton, Tooltip } from "@mui/material"
import { useEffect, useState } from "react"
import { useUi } from "../../context/uiContext"
import CreateIcon from '@mui/icons-material/Create';
import { styled } from '@mui/material/styles';
import { useSelection } from "../../context/selectionContext";


const StyledIconButton = styled(IconButton)({
    position:"absolute",
    bottom: '2%',
    left: 0,
})

let draw = new Sketch( {
    creationMode: 'single',
    visibleElements: {
        settingsMenu: false,
        selectionTools:{
            "lasso-selection": false,
            "rectangle-selection": false,
        },
        createTools:{
            point: false,
            polyline: false,
            circle: false,
            rectangle: false
        },

}}
)
export const DrawToolUi = ({view, layer})=>{
    const {sketch, setSketch} = useUi()
    const {selection, setSelection} = useSelection()
    const [color, setColor] = useState('primary')
    if(view && layer) {
        draw.view = view
        draw.visible = sketch
        draw.layer = layer
    }
    const handleClick = ()=>{
        setSketch && setSketch(!sketch)
        if(color === 'primary'){
            setColor('secondary')
        }
        else{
        setColor('primary')}
    }
    useEffect(()=>{
        if(!view) return
        if(!sketch){
            console.log('removig sketch')
            view.ui.remove(draw)
            layer.removeAll()
        }
        else if(sketch){
            console.log('creating sketch')
            view.ui.add(draw, {position: 'top-left', index: 1})
            draw.on('create', (e)=>{
                if(e.state === 'complete' && sketch){
                    let {latitude, longitude} = e.graphic.geometry.centroid
                    setSelection({...selection, latitude, longitude})
                    console.log(`Obtuvimos ${latitude} y ${longitude}`)
                }
            })
        }
        
    },[sketch]) //eslint-disable-line
    return(
        <Tooltip title="Draw a Polygon over the Map.">
        <StyledIconButton color={color} aria-label="drawing tools" onClick={handleClick}>
            <CreateIcon />
        </StyledIconButton>
        </Tooltip>
    )
}
import BasemapToogle from '@arcgis/core/widgets/BasemapToggle'
import Search from '@arcgis/core/widgets/Search'
import ScaleBar from "@arcgis/core/widgets/ScaleBar";
import CoordinateConversion from "@arcgis/core/widgets/CoordinateConversion";
import Sketch from "@arcgis/core/widgets/Sketch";
import { useUi } from '../../context/uiContext';

export const Mapui = ({view, graphicsLayer})=>{
    const {sketch:draw} = useUi()
    if (view){
    let ui = view.ui
    let mapToogle = new BasemapToogle({
        view: view,
        nextMap: 'hybrid'
    })
    
    let search = new Search({
        view: view,
    })
    let scaleBar = new ScaleBar({
        view: view,
        unit: 'metric'
    });
      let ccWidget = new CoordinateConversion({
          view: view,
          visible: true,
          container: null,
          visibleElements: {
              settingsButton: false,
              captureButton: false,
          }
        })
    const sketch = new Sketch(
        {
            layer: graphicsLayer,
            view: view,
            visible: draw,
            creationMode: 'single',
            visibleElements: {
                settingsMenu: false,
                selectionTools:{
                    "lasso-selection": false,
                    "rectangle-selection": false,
                },
                createTools:{
                    point: false,
                    polyline: false,
                    circle: false,
                    rectangle: false
                },
                
            }
        }
    )

      // Add widget to the bottom left corner of the view
      ui.add(mapToogle)
      ui.add(
          [search,ccWidget, sketch],
          {position: 'top-left', index: 0
        })
    ui.add(scaleBar, {
          position: "bottom-left"
        });
    ui.move(['zoom', mapToogle], 'top-right')}
    return(
        <>
        <div>
            hola
        </div>
        </>
    )
}
import { createContext, useCallback, useContext, useMemo, useState } from "react";


const InteracionContext = createContext()
export const useInteraction = ()=>useContext(InteracionContext)

export const InteractionProvider = ({children})=>{
    const [loading, setLoading] = useState(false)
    const [err, setErr] = useState()
    const [data, setData] = useState()
   

    const load = useMemo(()=>loading, [loading])
    const changeLoading = useCallback((val)=>{
        setLoading(val)
    },[load])//eslint-disable-line


    return(
        <InteracionContext.Provider value={{load, err, data, changeLoading, setErr, setData}}>
            {children}
        </InteracionContext.Provider>
    )
}
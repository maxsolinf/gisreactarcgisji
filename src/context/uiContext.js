import { useEffect, useState ,createContext, useCallback, useContext } from "react";

const Uicontext = createContext({})

export const useUi = () => useContext(Uicontext)

export const UiProvider = ({children}) =>{
    const [sketch, setSketch] = useState(false)
    const [search, setSearch] = useState(false)
    const [cc, setCC] = useState(false)
    const [zoom, setZoom]= useState()
    const [consulta, setConsulta] = useState(true)
    const [layerView, setLayerView] = useState(false)

    useEffect(()=>{
        if(consulta) return
        setTimeout(()=>setConsulta(true), 15000)
    },[consulta])

    return(
        <Uicontext.Provider value={{sketch, search, cc, zoom, consulta, layerView, setSketch, setSearch, setCC, setZoom, setConsulta, setLayerView}}>
            {children}
        </Uicontext.Provider>
    )
}
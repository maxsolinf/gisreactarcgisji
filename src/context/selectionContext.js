import { createContext, useContext, useState } from "react";

const SelectionContext = createContext({})
export const useSelection = ()=> useContext(SelectionContext)

export const SelectionProvider = ({children})=>{
    const [selection, setSelection] = useState({longitude: -70, latitude: 18})
    const [results, setResults] = useState()


    // useEffect(()=>{
    //     if(selection.longitude=== -70) return console.log('Seleccion aun sin cambios')
    //     console.log(`Selection is Latitude: ${selection.latitude} Longitude: ${selection.longitude}`)
    // },[selection])

    return(
        <SelectionContext.Provider value={{selection, results, setSelection, setResults}}>
            {children}
        </SelectionContext.Provider>
    )
}